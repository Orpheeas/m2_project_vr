// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "VR_Project/VRPawn_Base.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeVRPawn_Base() {}
// Cross Module References
	VR_PROJECT_API UClass* Z_Construct_UClass_AVRPawn_Base_NoRegister();
	VR_PROJECT_API UClass* Z_Construct_UClass_AVRPawn_Base();
	ENGINE_API UClass* Z_Construct_UClass_ADefaultPawn();
	UPackage* Z_Construct_UPackage__Script_VR_Project();
// End Cross Module References
	void AVRPawn_Base::StaticRegisterNativesAVRPawn_Base()
	{
	}
	UClass* Z_Construct_UClass_AVRPawn_Base_NoRegister()
	{
		return AVRPawn_Base::StaticClass();
	}
	struct Z_Construct_UClass_AVRPawn_Base_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_AVRPawn_Base_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_ADefaultPawn,
		(UObject* (*)())Z_Construct_UPackage__Script_VR_Project,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AVRPawn_Base_Statics::Class_MetaDataParams[] = {
		{ "HideCategories", "Navigation" },
		{ "IncludePath", "VRPawn_Base.h" },
		{ "ModuleRelativePath", "VRPawn_Base.h" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_AVRPawn_Base_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<AVRPawn_Base>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_AVRPawn_Base_Statics::ClassParams = {
		&AVRPawn_Base::StaticClass,
		DependentSingletons, ARRAY_COUNT(DependentSingletons),
		0x009000A0u,
		nullptr, 0,
		nullptr, 0,
		nullptr,
		&StaticCppClassTypeInfo,
		nullptr, 0,
		METADATA_PARAMS(Z_Construct_UClass_AVRPawn_Base_Statics::Class_MetaDataParams, ARRAY_COUNT(Z_Construct_UClass_AVRPawn_Base_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_AVRPawn_Base()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_AVRPawn_Base_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(AVRPawn_Base, 910230483);
	static FCompiledInDefer Z_CompiledInDefer_UClass_AVRPawn_Base(Z_Construct_UClass_AVRPawn_Base, &AVRPawn_Base::StaticClass, TEXT("/Script/VR_Project"), TEXT("AVRPawn_Base"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(AVRPawn_Base);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif

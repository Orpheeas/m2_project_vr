// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef VR_PROJECT_VRPawn_Base_generated_h
#error "VRPawn_Base.generated.h already included, missing '#pragma once' in VRPawn_Base.h"
#endif
#define VR_PROJECT_VRPawn_Base_generated_h

#define VR_Project_Source_VR_Project_VRPawn_Base_h_15_RPC_WRAPPERS
#define VR_Project_Source_VR_Project_VRPawn_Base_h_15_RPC_WRAPPERS_NO_PURE_DECLS
#define VR_Project_Source_VR_Project_VRPawn_Base_h_15_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesAVRPawn_Base(); \
	friend struct Z_Construct_UClass_AVRPawn_Base_Statics; \
public: \
	DECLARE_CLASS(AVRPawn_Base, ADefaultPawn, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/VR_Project"), NO_API) \
	DECLARE_SERIALIZER(AVRPawn_Base)


#define VR_Project_Source_VR_Project_VRPawn_Base_h_15_INCLASS \
private: \
	static void StaticRegisterNativesAVRPawn_Base(); \
	friend struct Z_Construct_UClass_AVRPawn_Base_Statics; \
public: \
	DECLARE_CLASS(AVRPawn_Base, ADefaultPawn, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/VR_Project"), NO_API) \
	DECLARE_SERIALIZER(AVRPawn_Base)


#define VR_Project_Source_VR_Project_VRPawn_Base_h_15_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API AVRPawn_Base(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(AVRPawn_Base) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AVRPawn_Base); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AVRPawn_Base); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AVRPawn_Base(AVRPawn_Base&&); \
	NO_API AVRPawn_Base(const AVRPawn_Base&); \
public:


#define VR_Project_Source_VR_Project_VRPawn_Base_h_15_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API AVRPawn_Base(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AVRPawn_Base(AVRPawn_Base&&); \
	NO_API AVRPawn_Base(const AVRPawn_Base&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AVRPawn_Base); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AVRPawn_Base); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(AVRPawn_Base)


#define VR_Project_Source_VR_Project_VRPawn_Base_h_15_PRIVATE_PROPERTY_OFFSET
#define VR_Project_Source_VR_Project_VRPawn_Base_h_12_PROLOG
#define VR_Project_Source_VR_Project_VRPawn_Base_h_15_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	VR_Project_Source_VR_Project_VRPawn_Base_h_15_PRIVATE_PROPERTY_OFFSET \
	VR_Project_Source_VR_Project_VRPawn_Base_h_15_RPC_WRAPPERS \
	VR_Project_Source_VR_Project_VRPawn_Base_h_15_INCLASS \
	VR_Project_Source_VR_Project_VRPawn_Base_h_15_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define VR_Project_Source_VR_Project_VRPawn_Base_h_15_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	VR_Project_Source_VR_Project_VRPawn_Base_h_15_PRIVATE_PROPERTY_OFFSET \
	VR_Project_Source_VR_Project_VRPawn_Base_h_15_RPC_WRAPPERS_NO_PURE_DECLS \
	VR_Project_Source_VR_Project_VRPawn_Base_h_15_INCLASS_NO_PURE_DECLS \
	VR_Project_Source_VR_Project_VRPawn_Base_h_15_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID VR_Project_Source_VR_Project_VRPawn_Base_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
